package com.egarcourse.auction;

import com.egarcourse.auction.datamodel.DataAccessEnvironment;
import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.AdministratorDTO;
import com.egarcourse.auction.domain.AdministratorAuction;
import com.egarcourse.auction.domain.users.Administrator;
import com.egarcourse.auction.services.AuthorizationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StartAndAuthorizationTests {

    ApplicationInitializer starter;
    DataAccessEnvironment environment;
    AuthorizationService authorizationService;

    @Before
    public void initialization() {
        starter = new ApplicationInitializer();
        starter.init(ApplicationInitializer.DataBaseType.FILES);
        environment = starter.getEnvironment();
        authorizationService = new AuthorizationService();
        authorizationService.init(environment);

        AdministratorDTO dto = new AdministratorDTO();
        dto.setAccountId(0);
        dto.setLogin("admin");
        dto.setPasswordHash(authorizationService.sha256("admin"));
        dto.setName("Creator");

        AdministratorDTO dto1 = new AdministratorDTO();
        dto1.setAccountId(1);
        dto1.setLogin("admin2");
        dto1.setPasswordHash(authorizationService.sha256("admin"));
        dto1.setName("Owner");

        AdministratorDTO dto2 = new AdministratorDTO();
        dto2.setAccountId(2);
        dto2.setLogin("admin3");
        dto2.setPasswordHash(authorizationService.sha256("otherpass"));
        dto2.setName("Moderator");

        DataAccess<AdministratorDTO> dao = environment.getAdminDAO();

        dao.insert(dto);
        dao.insert(dto1);
        dao.insert(dto2);
    }

    @Test
    public void testWriteAuthorization() {
        Assert.assertTrue(authorizationService.tryAuthorizeAdministrator("admin", "admin"));
    }

    @Test
    public void testWriteAuthorization2() {
        Assert.assertTrue(authorizationService.tryAuthorizeAdministrator("admin2", "admin"));
        Administrator administrator = authorizationService.authorizeAdministrator(new AdministratorAuction(), "admin2", "admin");
        Assert.assertEquals("admin2", administrator.getLogin());
        Assert.assertEquals("Owner", administrator.getName());
    }

    @Test
    public void testWrongAuthorization() {
        Assert.assertFalse(authorizationService.tryAuthorizeAdministrator("admin3", "admin"));
    }
}
