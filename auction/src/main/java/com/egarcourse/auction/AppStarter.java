package com.egarcourse.auction;

import com.egarcourse.auction.controllers.console.ConsoleController;

public class AppStarter {
    public static void main(String... args) {
        ApplicationInitializer starter = new ApplicationInitializer();

        if(args.length == 0) {
            starter.init(ApplicationInitializer.DataBaseType.FILES);
        } else if("postgresql".equals(args[0]) || "p".equals(args[0])) {
            starter.init(ApplicationInitializer.DataBaseType.POSTGRESQL);
        } else if("sqlite".equals(args[0]) || "s".equals(args[0])) {
            starter.init(ApplicationInitializer.DataBaseType.SQLITE);
        }

        ConsoleController controller = new ConsoleController();
        controller.init(
                starter.getService(),
                starter.getEnvironment(),
                starter.getAuction()
        );

        controller.start();
    }
}
