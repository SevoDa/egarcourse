package com.egarcourse.auction.controllers.console;

import sun.rmi.runtime.Log;

import java.io.*;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

public class ConsoleView {
    private static final Logger logger = Logger.getLogger(ConsoleView.class.getName());

    PrintWriter out;
    BufferedReader in;

    public void init(PrintWriter out, BufferedReader in) {
        this.out = out;
        this.in = in;
    }

    public int mainMenu() {
        try {
            final String menuString =
                    "Hello, Guest!\n" +
                            "Did you want authorize in your account, register or continue as guest?\n" +
                            "1 - Authorize\n" +
                            "2 - Register\n" +
                            "3 - Continue as Guest";

            System.out.println(menuString);
            int ans = Integer.valueOf(in.readLine().trim());
            if (ans < 1 || ans > 3) return -1;
            return ans;
        } catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalArgumentException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public int guestMenu(String name) {
        try {
            String menu = "Guest. " + name + "\n" +
                    "0 - Close" + "\n" +
                    "1 - See all categories\n" +
                    "2 - See all lots by category\n" +
                    "3 - See all bets at lot\n";
            System.out.println(menu);
            int chose = Integer.valueOf(in.readLine().trim());
            if (chose < 0 || chose > 3) return -1;
            return chose;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalArgumentException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public int clientMenu(String name) {
        try {
            String menu = "Client. " + name + "\n" +
                    "0 - Close" + "\n" +
                    "1 - See all categories\n" +
                    "2 - See all lots by category\n" +
                    "3 - See all bets at lot\n" +
                    "4 - See my lots\n" +
                    "5 - See my bets";
            System.out.println(menu);
            int chose = Integer.valueOf(in.readLine().trim());
            if (chose < 0 || chose > 5) return -1;
            return chose;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalArgumentException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public int adminMenu(String name) {
        try {
            String menu = "Admin. " + name + "\n" +
                    "0 - Close" + "\n" +
                    "1 - See all categories\n" +
                    "2 - See all lots by category\n" +
                    "3 - See all bets at lot\n" +
                    "4 - See statistics\n" +
                    "5 - See name sorted clients list\n" +
                    "6 - See birth sorted clients list";
            System.out.println(menu);
            int chose = Integer.valueOf(in.readLine().trim());
            if (chose < 0 || chose > 6) return -1;
            return chose;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalArgumentException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public int authorizationMenu() {
        try {
            final String authChoseStr = "Choose your account's type:\n" +
                    "1 - Client\n" +
                    "2 - Administrator";
            System.out.println(authChoseStr);
            int ans = Integer.valueOf(in.readLine().trim());
            if (ans < 1 || ans > 2) return -1;
            return ans;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalArgumentException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public String guestContinue() {
        try {
            System.out.println("Enter your name, please: ");
            String name = in.readLine().trim();
            return name;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public LoginAnswer authorization() {
        try {
            final String loginStr =
                    "Hello! Enter your data:\n" +
                            "login: ";

            final String passStr = "password: ";

            System.out.println(loginStr);
            String login = in.readLine().trim();
            System.out.println(passStr);
            String pass = in.readLine().trim();

            LoginAnswer answer = new LoginAnswer();
            answer.login = login;
            answer.password = pass;
            return answer;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public void successAuthorization(String name) {
        String success = "Hi, " + name + "! You entered your account \n";
        System.out.println(success);
    }

    public void failedAuthorization() {
        String fail = "Login or password wrong!\n";
        System.out.println(fail);
    }

    public String registerEnteredLogin() {
        try {
            System.out.println("login: ");
            String login = in.readLine().trim();
            return login;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public void failedUniqueLogin() {
        String fail = "Your login already occupied";
        System.out.println(fail);
    }

    public RegisterAnswerClient registerAfterEnteredLogin(String login) {
        RegisterAnswerClient answer = new RegisterAnswerClient();

        try {
            System.out.println("password: ");
            String password = in.readLine().trim();
            System.out.println("name: ");
            String name = in.readLine().trim();
            System.out.println("birth: ");
            String birthDay = in.readLine().trim();
            System.out.println("money: ");
            String money = in.readLine().trim();

            answer.login = login;
            answer.name = name;
            answer.password = password;
            answer.money = Long.valueOf(money);
            answer.birthDay = Date.valueOf(birthDay);

            return answer;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalArgumentException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public void successRegister(String name) {
        String success = "Hi, " + name + "! Now you can enter in your account! \n";
        System.out.println(success);
    }

    public int printCategories(List<String> categories) {
        for(int i = 0; i < categories.size(); ++i) {
            System.out.println((i+1) + " " + categories.get(i));
        }
        System.out.println("-- enter 1 to exit --");
        return getExit();
    }

    public int choseCategory(int max) {
        try {
            System.out.println("Chose interested category: ");
            int chose = Integer.valueOf(in.readLine().trim());

            if (chose < 1 || chose > max) return -1;
            return chose;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalArgumentException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public int printLotsByCategory(List<String> lots) {
        for(int i = 0; i < lots.size(); ++i) {
            System.out.println((i+1) + " " + lots.get(i));
        }
        System.out.println("-- enter 1 to exit --");
        return getExit();
    }

    public int choseLot(int max) {
        try {
            System.out.println("Chose interested lot: ");
            int chose = Integer.valueOf(in.readLine().trim());

            if (chose < 1 || chose > max) return -1;
            return chose;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalArgumentException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public int printBetsByLot(List<String> bets) {
        for(int i = 0; i < bets.size(); ++i) {
            System.out.println((i+1) + " " + bets.get(i));
        }
        System.out.println("-- enter 1 to exit --");
        return getExit();
    }

    public int printClientsLots(List<String> lots, Map<Integer, List<String>> bets) {
        for(int i = 0; i < lots.size(); ++i) {
            System.out.println("lot " + (i+1) + lots.get(i));
            if(bets.containsKey(i)) {
                List<String> curBets = bets.get(i);
                for(int j = 0; j < Math.min(3, curBets.size()); ++j) {
                    System.out.println("  bet " + (j+1) + curBets.get(j));
                }
            }
        }
        System.out.println("-- enter 1 to exit --");
        return getExit();
    }

    public int printClientsBets(List<String> bets) {
        for(int i = 0; i < bets.size(); ++i) {
            System.out.println((i+1) + " " + bets.get(i));
        }
        System.out.println("-- enter 1 to exit --");
        return getExit();
    }

    public int printStatistics(List<String> stats) {
        for(int i = 0; i < stats.size(); ++i) {
            System.out.println((i+1) + " " + stats.get(i));
        }
        System.out.println("-- enter 1 to exit --");
        return getExit();
    }

    public int printClients(List<String> clients) {
        for(int i = 0; i < clients.size(); ++i) {
            System.out.println((i+1) + " " + clients.get(i));
        }
        System.out.println("-- enter 1 to exit --");
        return getExit();
    }

    public int getExit() {
        try {
            int enter = Integer.valueOf(in.readLine().trim());

            return enter == 1 ? 1 : -1;
        }
        catch (IOException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IllegalArgumentException e) {
            logger.warning("Error in view!");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    class LoginAnswer {
        private String login;
        private String password;

        public String getLogin() { return login; }
        public String getPassword() { return password; }
    }

    class RegisterAnswerClient {
        private String name;
        private String login;
        private String password;
        private Date birthDay;
        private long money;

        public String getName() {
            return name;
        }

        public String getLogin() {
            return login;
        }

        public String getPassword() {
            return password;
        }

        public Date getBirthDay() {
            return birthDay;
        }

        public long getMoney() {
            return money;
        }
    }

}
