package com.egarcourse.auction.controllers.console;

import com.egarcourse.auction.datamodel.DataAccessEnvironment;
import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.ClientDTO;
import com.egarcourse.auction.domain.AdministratorAuction;
import com.egarcourse.auction.domain.GuestAuction;
import com.egarcourse.auction.domain.users.Administrator;
import com.egarcourse.auction.domain.users.Client;
import com.egarcourse.auction.domain.users.Guest;
import com.egarcourse.auction.domain.users.User;
import com.egarcourse.auction.domain.users.adminentity.StatisticsEntity;
import com.egarcourse.auction.domain.users.cliententity.Bet;
import com.egarcourse.auction.domain.users.cliententity.Lot;
import com.egarcourse.auction.services.AuthorizationService;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

public class ConsoleController {
    private static final Logger logger = Logger.getLogger(ConsoleController.class.getName());

    private ConsoleView view = new ConsoleView();
    private AuthorizationService authService;
    private DataAccessEnvironment dataAccessEnvironment;
    private AdministratorAuction auction;

    private Guest guest;
    private Administrator admin;
    private Client client;
    private List<String> categories = new ArrayList<>();
    private Map<String, List<Lot>> lots = new HashMap<>();

    private PrintWriter out;
    private BufferedReader in;

    private enum UserStatus {
        GUEST, CLIENT, ADMIN
    }

    UserStatus status = UserStatus.GUEST;

    public void init(AuthorizationService authService,
                     DataAccessEnvironment environment,
                     AdministratorAuction auction) {
        this.auction = auction;
        this.authService = authService;
        this.dataAccessEnvironment = environment;
        out = new PrintWriter(System.out);
        in = new BufferedReader(new InputStreamReader(System.in));
        view.init(out, in);

        update();
    }

    public void start() {
        int ans = view.mainMenu();
        do {
            switch (ans) {
                case -1: {
                    ans = view.mainMenu();
                    break;
                }
                case 1: {
                    authorizationSwitcher();
                    break;
                }
                case 2: {
                    register();
                    break;
                }
                case 3: {
                    guestContinue();
                    break;
                }
            }
        } while(ans == -1);
    }

    private void authorizationSwitcher() {
        int ans = view.authorizationMenu();
        do {
            switch(ans) {
                case 1: {
                    authClient();
                    break;
                }
                case 2: {
                    authAdmin();
                    break;
                }
            }
        } while(ans == -1);
    }

    private void authAdmin() {
        ConsoleView.LoginAnswer loginAnswer = view.authorization();
        if(!authService.tryAuthorizeAdministrator(loginAnswer.getLogin(), loginAnswer.getPassword())) {
            view.failedAuthorization();
            return;
        } else {
            admin = authService.authorizeAdministrator(auction, loginAnswer.getLogin(), loginAnswer.getPassword());
            view.successAuthorization(admin.getName());
            status = UserStatus.ADMIN;
            adminMenu();
        }
    }

    private void authClient() {
        ConsoleView.LoginAnswer loginAnswer = view.authorization();
        if(!authService.tryAuthorizeClient(loginAnswer.getLogin(), loginAnswer.getPassword())) {
            view.failedAuthorization();
            return;
        } else {
            client = authService.authorizeClient(auction, loginAnswer.getLogin(), loginAnswer.getPassword());
            view.successAuthorization(client.getName());
            status = UserStatus.CLIENT;
            clientMenu();
        }
    }

    private String testClientLogin() {
        String login = view.registerEnteredLogin();
        if(!authService.containsClientLogin(login)) return login;
        else return null;
    }

    private String testAdminLogin() {
        String login = view.registerEnteredLogin();
        if(!authService.containsAdminLogin(login)) return login;
        else return null;
    }

    private void register() {
        String login = testClientLogin();
        if(login == null) {
            view.failedUniqueLogin();
            return;
        }

        ConsoleView.RegisterAnswerClient registerAnswerClient =
                view.registerAfterEnteredLogin(login);

        DataAccess<ClientDTO> dao = dataAccessEnvironment.getClientDAO();
        ClientDTO dto = new ClientDTO();
        dto.setAccountId(0);
        dto.setLogin(registerAnswerClient.getLogin());
        dto.setName(registerAnswerClient.getName());
        dto.setPasswordHash(authService.sha256(registerAnswerClient.getPassword()));
        dto.setBirthDay(registerAnswerClient.getBirthDay());
        dto.setMoney(registerAnswerClient.getMoney());

        dao.insert(dto);

        view.successRegister(dto.getName());
        authClient();
    }

    private void guestContinue() {
        String name = view.guestContinue();
        guest = new Guest(auction);
        guest.setName(name);
        status = UserStatus.GUEST;
        guestMenu();
    }

    private void guestMenu() {
        int ans;
        boolean end = false;
        while(!end) {
            ans = view.guestMenu(guest.getName());
            switch (ans) {
                case -1:{
                    break;
                }
                case 0: {
                    end = true;
                    break;
                }
                case 1: {
                    printAllCategories();
                    break;
                }
                case 2: {
                    printAllLotsAtCategory();
                    break;
                }
                case 3: {
                    printAllBetsAtLot();
                    break;
                }
            }
        }
    }

    private void clientMenu() {
        boolean end = false;
        int ans;
        while(!end) {
            ans = view.clientMenu(client.getName());
            switch (ans) {
                case -1:{
                    break;
                }
                case 0: {
                    end = true;
                    break;
                }
                case 1: {
                    printAllCategories();
                    break;
                }
                case 2: {
                    printAllLotsAtCategory();
                    break;
                }
                case 3: {
                    printAllBetsAtLot();
                    break;
                }
                case 4: {
                    printClientsLots();
                    break;
                }
                case 5: {
                    printClientsBets();
                    break;
                }
            }
        }
    }

    private void adminMenu() {
        try {
            int ans;
            boolean end = false;
            while (!end) {
                ans = view.adminMenu(admin.getName());
                switch (ans) {
                    case -1: {
                        break;
                    }
                    case 0: {
                        end = true;
                        break;
                    }
                    case 1: {
                        printAllCategories();
                        break;
                    }
                    case 2: {
                        printAllLotsAtCategory();
                        break;
                    }
                    case 3: {
                        printAllBetsAtLot();
                        break;
                    }
                    case 4: {
                        printStatistics();
                        break;
                    }
                    case 5: {
                        printNameSortedClients();
                        break;
                    }
                    case 6: {
                        printBirthSortedClients();
                        break;
                    }
                }
                out.flush();
            }
            in.close();
            out.close();
        }
        catch (IOException e) {
            logger.severe("IO exception!");
        }
    }

    private void printClientsLots() {
        List<Lot> lots = client.getLots();
        Map<Integer, List<String>> betsAtLots = new HashMap<>();
        List<String> lotsStrings = new ArrayList<>();
        for(int i = 0; i < lots.size(); ++i) {
            Lot lot = lots.get(i);
            betsAtLots.put(i, new ArrayList<>());
            lotsStrings.add(lot.getName() + " " + lot.getCategory());
            Deque<Bet> betsStack = lot.getBetsStack();
            for(int j = 0; j < betsStack.size(); ++j) {
                Bet bet = betsStack.pop();
                betsAtLots.get(i).add(bet.getPrice() + " " + bet.getOwner());
            }
        }

        int ans = view.printClientsLots(lotsStrings, betsAtLots);

        while(ans != 1) {
            ans = view.getExit();
        }
    }

    private void printStatistics() {
        List<StatisticsEntity> stats = admin.getStatistics();
        List<String> outputStats = new ArrayList<>();
        stats.forEach(x -> outputStats.add(x.getClientName() +
                " bets: "
                + x.getActiveBetsCount() + " lots: "
                + x.getLotsCount())
        );

        int ans = view.printStatistics(outputStats);

        while(ans != 1) {
            ans = view.getExit();
        }
    }

    private void printNameSortedClients() {
        List<Client> clients = admin.getNameSortedClients();
        List<String> clientsOutput = new ArrayList<>();
        clients.forEach(x -> clientsOutput.add(
                x.getName() + " " +
                        x.getLogin() + " " +
                        x.getBirthDay() + " " +
                        x.getId()
        ));

        int ans = view.printClients(clientsOutput);
        while(ans != 1) {
            ans = view.getExit();
        }
    }

    private void printBirthSortedClients() {
        List<Client> clients = admin.getBirthSortedClients();
        List<String> clientsOutput = new ArrayList<>();
        clients.forEach(x -> clientsOutput.add(
                x.getName() + " " +
                        x.getLogin() + " " +
                        x.getBirthDay() + " " +
                        x.getId()
        ));

        int ans = view.printClients(clientsOutput);
        while(ans != 1) {
            ans = view.getExit();
        }
    }

    private void printClientsBets() {
        List<Bet> bets = client.getBets();
        List<String> betsString = new ArrayList<>();
        bets.forEach(x -> betsString.add(x.getPrice() + " " + x.getLot().getName() + " " + x.getLot().getCategory()));

        int ans = view.printClientsBets(betsString);
        while(ans != 1) {
            ans = view.getExit();
        }
    }

    private void printAllCategories() {
        int ans = view.printCategories(categories);
        while(ans != 1) {
            ans = view.getExit();
        }
    }

    private void printAllLotsAtCategory() {
        int category = view.choseCategory(categories.size())-1;
        if(category == -2) {
            return;
        }
        List<Lot> curLots = lots.get(categories.get(category));
        List<String> output = new ArrayList<>();
        curLots.forEach(
                x -> output.add(x.getName() + " " + x.getLotId() + " " + x.getLastBet().getPrice())
        );

        int ans = view.printLotsByCategory(output);
        while(ans != 1) {
            ans = view.getExit();
        }
    }

    private void printAllBetsAtLot() {
        int category = view.choseCategory(categories.size())-1;
        if(category == -2) return;
        int lot = view.choseLot(lots.get(category).size())-1;
        if(lot == -2) return;
        Deque<Bet> stack = lots.get(category).get(lot).getBetsStack();
        List<String> output = new ArrayList<>();
        stack.forEach(
                x -> output.add(x.getPrice() + " " + x.getCreationTime() + " " + x.getOwner().getName())
        );
        int ans = view.printBetsByLot(output);
        while(ans != 1) ans = view.getExit();
    }

    private void update() {
        GuestAuction guestAuction = auction;
        Set<String> categoriesSet = new HashSet<String>();
        lots = new HashMap<>();
        guestAuction.getLots().forEach(x -> {
            String category = x.getCategory();
            categoriesSet.add(category);
            if(!lots.containsKey(category)) lots.put(category, new ArrayList<>());
            lots.get(category).add(x);
        });
        categories.addAll(categoriesSet);
    }
}
