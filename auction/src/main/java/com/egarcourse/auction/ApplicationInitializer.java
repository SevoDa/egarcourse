package com.egarcourse.auction;

import com.egarcourse.auction.datamodel.DataAccessEnvironment;
import com.egarcourse.auction.datamodel.JsonFilesEnvironment;
import com.egarcourse.auction.datamodel.PostgresEnvironment;
import com.egarcourse.auction.datamodel.SQLiteEnvironment;
import com.egarcourse.auction.domain.AdministratorAuction;
import com.egarcourse.auction.services.AuthorizationService;

import java.util.logging.Logger;

/**
 * Application Initializer and starter
 */
public class ApplicationInitializer {
    private static final Logger logger = Logger.getLogger(ApplicationInitializer.class.getName());

    enum DataBaseType {
        SQLITE, POSTGRESQL, FILES;
    }

    private DataAccessEnvironment environment;
    private AuthorizationService service;
    private AdministratorAuction auction;

    public void init(DataBaseType type) {
        auction = new AdministratorAuction();
        switch (type) {
            case FILES: {
                filesInit();
                break;
            }
            case SQLITE: {
                sqliteInit();
                break;
            }
            case POSTGRESQL: {
                postgresInit();
                break;
            }
        }
        service = new AuthorizationService();
        service.init(environment);
    }

    public DataAccessEnvironment getEnvironment() {
        return environment;
    }

    public AuthorizationService getService() {
        return service;
    }

    public AdministratorAuction getAuction() {
        return auction;
    }

    private void filesInit() {
        environment = new JsonFilesEnvironment();
        environment.init();
    }

    private void sqliteInit() {
        environment = new SQLiteEnvironment();
        environment.init();
    }

    private void postgresInit() {
        environment = new PostgresEnvironment();
        environment.init();
    }
}
