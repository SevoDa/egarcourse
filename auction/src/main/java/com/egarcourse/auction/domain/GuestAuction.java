package com.egarcourse.auction.domain;

import com.egarcourse.auction.domain.users.cliententity.Bet;
import com.egarcourse.auction.domain.users.cliententity.Lot;

import java.util.*;
import java.util.logging.Logger;

/**
 * Guest's auction view
 * Provides an interface auction to guests
 */
public class GuestAuction {
    private static final Logger logger = Logger.getLogger(GuestAuction.class.getName());

    protected Set<String> categories = new HashSet<>();
    protected Map<String, Set<Lot>> lots = new HashMap<>();
    protected Set<Bet> bets = new HashSet<>();

    /**
     * Returns all active lots
     * @return
     */
    public List<Lot> getLots(){
        List<Lot> allLots = new ArrayList<>();
        for(String category : categories) {
            allLots.addAll(lots.get(category));
        }
        return allLots;
    }

    /**
     * Returns all lots for category
     * Call containsCategory(String) before
     * @param category
     * @return lots
     */
    public Set<Lot> getLotsForCategory(String category) {
        if(!containsCategory(category)) throw new IllegalArgumentException("Auction did not contains that category");
        return lots.get(category);
    }

    /**
     * Call this method before getLotsForCategory(String)
     * @param category
     * @return
     */
    public boolean containsCategory(String category) {
        return categories.contains(category);
    }

    /**
     * Returns all active bets
     * @return
     */
    public List<Bet> getBets() {
        List<Bet> betsList = new ArrayList<>();
        betsList.addAll(bets);
        return betsList;
    }
}
