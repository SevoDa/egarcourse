package com.egarcourse.auction.domain.users.adminentity;

import com.egarcourse.auction.domain.users.Client;

/**
 * Element of statistic, that provides to administrators
 * Contains high-weight Client object
 */
public class StatisticsEntity {
    private Client client;
    private String clientName;
    private int lotsCount;
    private int activeBetsCount;

    public StatisticsEntity(Client client) {
        this.client = client;
        this.lotsCount = client.countLots();
        this.activeBetsCount = client.countActiveBets();
        this.clientName = client.getName();
    }

    public Client getClient() { return client; }
    public String getClientName() { return clientName; }
    public int getLotsCount() { return lotsCount; }
    public int getActiveBetsCount() { return activeBetsCount; }

    @Override
    public String toString() {
        return "name: " + clientName + " lots: " + lotsCount + " active bets: " + activeBetsCount;
    }
}
