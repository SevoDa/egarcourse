package com.egarcourse.auction.domain;

import com.egarcourse.auction.domain.users.Client;
import com.egarcourse.auction.domain.users.cliententity.Bet;
import com.egarcourse.auction.domain.users.cliententity.Lot;

import java.util.*;
import java.util.logging.Logger;

/**
 * Client's auction view
 * Provides an interface auction to clients
 */
public class ClientAuction extends GuestAuction {
    private static final Logger logger = Logger.getLogger(ClientAuction.class.getName());

    protected List<Bet> betsHistory = new ArrayList<>();
    protected Set<Client> clients = new HashSet<>();

    /**
     * Add lot in auction registry
     * @param lot
     */
    public void addLot(Lot lot) {
        String lotCategory = lot.getCategory();

        if(!this.containsCategory(lotCategory)) {
            this.categories.add(lotCategory);
            lots.put(lotCategory, new TreeSet<>());
        }

        Set<Lot> allLots = getLotsForCategory(lotCategory);
        allLots.add(lot);
    }

    /**
     * Tries put bet on lot.
     * If argument is bigger than current, returns true else returns false
     * @param bet
     * @return flag
     */
    public boolean checkBet(Bet bet) {
        Lot candidateLot = bet.getLot();

        if(candidateLot == null) return false;
        if(candidateLot.getLastBet() == null) return true;

        Bet lastBet = candidateLot.getLastBet();
        return bet.getPrice() > lastBet.getPrice();
    }

    /**
     * Put bet on lot.
     * You should call checkBet(Bet) before to test opportunity
     * @param bet
     */
    public void addBet(Bet bet) {
        if(!checkBet(bet)) throw new IllegalArgumentException("bet's price should be more than previous");
        Lot candidateLot = bet.getLot();
        Bet lastBet = candidateLot.getLastBet();
        lastBet.cancel();
        this.bets.add(bet);
    }

    /**
     * Returns online clients from registry
     * @return clients
     */
    public Set<Client> getClients() {
        return clients;
    }

    /**
     * add client to auction registry
     * @param client
     */
    public void addClient(Client client) {
        this.clients.add(client);
    }

    /**
     * remove client from auction registry
     * @param client
     */
    public void removeClient(Client client) {
        this.clients.remove(client);
    }

    /**
     * Returns history of all bets, while auction is running
     * @return list of bets
     */
    public List<Bet> getBetsHistory() {
        return betsHistory;
    }

    /**
     * confirm bet
     * makes deal valid
     * Buyer gets his lot
     * Seller gets his money
     * @param bet
     */
    public void commitBet(Bet bet) {
        releaseLot(bet.getLot());
        bet.commit();
        bets.remove(bet);
        // write in history
    }

    /**
     * Its happens in commitBet(Bet)
     * Remove lot from active list
     * @param lot
     */
    private void releaseLot(Lot lot) {
        String lotCategory = lot.getCategory();
        this.lots.get(lotCategory).remove(lot);
        if(lots.get(lotCategory).isEmpty()) {
            categories.remove(lotCategory);
        }
    }

    /**
     * Remove lot from auction (error/non typical case)
     * @param lot
     */
    public void cancelLot(Lot lot) {
        releaseLot(lot);
        Bet lastBet = lot.getLastBet();
        lastBet.cancel();
        lot.cancel();
        bets.remove(lastBet);
    }
}
