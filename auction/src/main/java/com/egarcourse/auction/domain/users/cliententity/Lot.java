package com.egarcourse.auction.domain.users.cliententity;

import java.util.Date;
import java.util.Deque;

/**
 *  Entity that provides all operations with lot
 */
public class Lot {
    private String name;
    private boolean active;
    private Date creationTime;
    private Date closingTime;
    private Bet lastBet;
    private long lotId;
    private String category;
    private Deque<Bet> betsStack;

    public boolean isActive() {
        return active;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public Date getClosingTime() {
        return closingTime;
    }

    /**
     * Makes lot inactive
     */
    public void cancel() {
        active = false;
    }

    public Bet getLastBet() {
        return lastBet;
    }

    public void setLastBet(Bet bet) {
        betsStack.push(bet);
        this.lastBet = bet;
    }

    public long getLotId() {
        return lotId;
    }

    public void setLotId(long lotId) {
        this.lotId = lotId;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public void setClosingTime(Date closingTime) {
        this.closingTime = closingTime;
    }

    public Deque<Bet> getBetsStack() {
        return betsStack;
    }

    public void setBetsStack(Deque<Bet> betsStack) {
        this.betsStack = betsStack;
    }
}
