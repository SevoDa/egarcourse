package com.egarcourse.auction.domain.users.cliententity;

import com.egarcourse.auction.domain.users.Client;

import java.util.Date;

/**
 * Entity that provides all operations with bet
 */
public class Bet {
    private Lot lot;
    private long price;
    private boolean active;
    private Date creationTime;
    private Client owner;
    private long betId;

    public boolean isActive() {
        return active;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    /**
     * Cancel makes bet inactive
     */
    public void cancel() {
        active = false;
        owner.cancelBet(this);
    }

    /**
     * Confirm selling
     */
    public void commit() {
        if(!this.isActive()) {
            throw new IllegalArgumentException("Bet should be active to commit");
        }
        Lot lot = this.getLot();
        lot.cancel();
        this.cancel();

        owner.addLot(lot);
    }

    /**
     * Returns bet's price
     * @return
     */
    public long getPrice() {
        return price;
    }

    public Lot getLot() {
        return lot;
    }

    public Client getOwner() {
        return owner;
    }

    public long getBetId() {
        return betId;
    }

    public void setBetId(long betId) {
        this.betId = betId;
    }
}
