package com.egarcourse.auction.domain.users;

import com.egarcourse.auction.domain.AdministratorAuction;
import com.egarcourse.auction.domain.users.adminentity.StatisticsEntity;
import com.egarcourse.auction.domain.users.cliententity.Bet;
import com.egarcourse.auction.domain.users.cliententity.Lot;

import java.util.*;

/**
 * Provides administrator account entity
 * Contains auth info: login && account id
 */
public class Administrator implements User {
    private boolean authorized = false;
    private String name;
    private String login;
    private AdministratorAuction auction;
    private long id;

    public Administrator(AdministratorAuction auction) {
        this.auction = auction;
    }

    /**
     * Returns account id
     * @return id
     */
    public long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return (int)id;
    }

    @Override
    public boolean equals(Object oth) {
        if(!(oth instanceof Administrator)) return false;
        Administrator othClient = (Administrator) oth;
        if(this.hashCode() != othClient.hashCode()) return false;

        return id == othClient.getId() && name.equals(othClient.getName()) && login.equals(othClient.getName());
    }

    @Override
    public boolean isAuthorized() {
        return authorized;
    }

    /**
     * Set administrator active after authorize
     */
    public void authorize() {
        this.authorized = true;
    }

    /**
     * Set administrator inactive after exit
     */
    public void unAuthorize() {
        this.authorized = false;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    @Override
    public List<Bet> getAllBets() {
        return auction.getBets();
    }

    @Override
    public List<Lot> getAllLots() {
        return auction.getLots();
    }

    @Override
    public List<Lot> getLotsForCategory(String category) {
        List<Lot> lots = new ArrayList<>();
        lots.addAll(auction.getLotsForCategory(category));
        return lots;
    }

    /**
     * Returns history of all bets, did while auction is active
     * @return betsHistory
     */
    public List<Bet> getBetsHistory() {
        return auction.getBetsHistory();
    }

    /**
     * Returns statistics of clients activity
     * @return statistics
     */
    public List<StatisticsEntity> getStatistics() {
        List<StatisticsEntity> stats = new ArrayList<>();
        Set<Client> clients = this.auction.getClients();

        for(Client client : clients) {
            StatisticsEntity entity = new StatisticsEntity(client);
            stats.add(entity);
        }

        return stats;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Client> getNameSortedClients() {
        Set<Client> clientsSet = this.auction.getClients();

        List<Client> clients = new ArrayList<>();
        clients.addAll(clientsSet);
        clients.sort(new NameComparator());

        return clients;
    }

    public List<Client> getBirthSortedClients() {
        Set<Client> clientsSet = this.auction.getClients();

        List<Client> clients = new ArrayList<>();
        clients.addAll(clientsSet);
        clients.sort(new BirthComparator());

        return clients;
    }

    private class NameComparator implements Comparator<Client> {

        @Override
        public int compare(Client o1, Client o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    private class BirthComparator implements Comparator<Client> {

        @Override
        public int compare(Client o1, Client o2) {
            return o1.getBirthDay().compareTo(o2.getBirthDay());
        }
    }
}
