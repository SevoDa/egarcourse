package com.egarcourse.auction.domain.users;

import com.egarcourse.auction.datamodel.entities.ClientDTO;
import com.egarcourse.auction.domain.ClientAuction;
import com.egarcourse.auction.domain.users.cliententity.Bet;
import com.egarcourse.auction.domain.users.cliententity.Lot;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Provides client account entity
 * Contains auth info: login && account id
 */
public class Client implements User {
    private boolean authorized = false;
    private String name;
    private String login;
    private List<Lot> lots;
    private List<Bet> bets;
    private Set<Bet> activeBets;
    private ClientAuction auction;
    private Date birthDay;
    private long id;
    private long money;

    public Client(ClientAuction auction) {
        this.auction = auction;
    }

    /**
     * Returns account id
     * @return id
     */
    public long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return (int)id;
    }

    @Override
    public boolean equals(Object oth) {
        if(!(oth instanceof Client)) return false;
        Client othClient = (Client) oth;
        if(this.hashCode() != othClient.hashCode()) return false;

        return id == othClient.getId() && name.equals(othClient.getName()) && login.equals(othClient.getName());
    }

    @Override
    public boolean isAuthorized() {
        return authorized;
    }

    /**
     * Set client active after authorize
     */
    public void authorize() {
        this.authorized = true;
    }

    /**
     * Set client inactive after exit
     */
    public void unAuthorize() {
        this.authorized = false;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    @Override
    public List<Bet> getAllBets() {
        return auction.getBets();
    }

    @Override
    public List<Lot> getAllLots() {
        return auction.getLots();
    }

    @Override
    public List<Lot> getLotsForCategory(String category) {
        List<Lot> lots = new ArrayList<>();
        lots.addAll(auction.getLotsForCategory(category));
        return lots;
    }

    /**
     * Returns all lots, that client has
     * @return lots
     */
    public List<Lot> getLots() {
        return lots;
    }

    /**
     * Add lot to client's ownership
     * @param lot
     */
    public void addLot(Lot lot) {
        this.lots.add(lot);
    }

    public int countLots() {
        return lots.size();
    }

    /**
     * Returns all bets, what did user while auction is active
     * @return bets
     */
    public List<Bet> getBets() {
        return bets;
    }

    /**
     * Returns bets, that active now and can be commit or cancel
     * @return bets
     */
    public Set<Bet> getActiveBets() {
        return activeBets;
    }

    /**
     * Adds bet for account. Did not removes money
     * You should call checkBet before
     * @param bet
     */
    public void addBet(Bet bet) {
        if(!checkBet(bet)) throw new IllegalArgumentException("Didn't called checkBet(Bet) before addBet(Bet)");
        auction.addBet(bet);
        activeBets.add(bet);
        bets.add(bet);
    }

    /**
     * Call this method before addBet(Bet)
     * Tries did bet
     * @param bet
     * @return checked
     */
    public boolean checkBet(Bet bet) {
        long price = bet.getPrice();
        return (price <= money) && auction.checkBet(bet);
    }

    public int countBets() {
        return bets.size();
    }

    public int countActiveBets() {
        return activeBets.size();
    }

    /**
     * Remove bet from list of active bets
     * @param bet
     */
    public void cancelBet(Bet bet) {
        activeBets.remove(bet);
        addMoney(bet.getPrice());
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        final long magicNumber = 2l;
        if(money < 0 || money >= Long.MAX_VALUE / magicNumber ) throw new IllegalArgumentException("Invalid money parameter");
        this.money = money;
    }

    /**
     * subs money from account
     * @param money
     */
    public void removeMoney(long money) {
        if(money < 0 || money >= Long.MAX_VALUE - this.money - 1) throw new IllegalArgumentException("Invalid money parameter");
        this.money -= money;
    }

    /**
     * add money to account
     * @param money
     */
    public void addMoney(long money) {
        if(money < 0 || money >= Long.MAX_VALUE - this.money - 1) throw new IllegalArgumentException("Invalid money parameter");
        this.money += money;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setLots(List<Lot> lots) {
        this.lots = lots;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public void setActiveBets(Set<Bet> activeBets) {
        this.activeBets = activeBets;
    }

    public void setAuction(ClientAuction auction) {
        this.auction = auction;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }
}
