package com.egarcourse.auction.domain;

import com.egarcourse.auction.domain.users.Administrator;
import com.egarcourse.auction.domain.users.Client;
import com.egarcourse.auction.domain.users.adminentity.StatisticsEntity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;


/**
 * Administrator's auction view
 * Provides an interface auction to administrators
 */
public class AdministratorAuction extends ClientAuction {
    private static final Logger logger = Logger.getLogger(AdministratorAuction.class.getName());

    private Set<Administrator> administrators = new HashSet<>();

    /**
     * Returns all online administrators from registry
     * @return administrators
     */
    public Set<Administrator> getAdministrators() {
        return administrators;
    }

    /**
     * Add administrator in online registry
     * @param administrator
     */
    public void addAdministrator(Administrator administrator) {
        this.administrators.add(administrator);
    }

    /**
     * Remove administrator from online registry
     * @param administrator
     */
    public void removeAdministrator(Administrator administrator) {
        this.administrators.remove(administrator);
    }

    /**
     * Provides client's statistics info
     * @return statistics
     */
    public List<StatisticsEntity> getStatistics() {
        List<StatisticsEntity> statistics = new ArrayList<>();
        for(Client client : clients) {
            statistics.add(new StatisticsEntity(client));
        }
        return statistics;
    }
}
