package com.egarcourse.auction.domain.users;

import com.egarcourse.auction.domain.GuestAuction;
import com.egarcourse.auction.domain.users.cliententity.Bet;
import com.egarcourse.auction.domain.users.cliententity.Lot;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides basic entity of anonymous user
 */
public class Guest implements User {
    private String name;
    private GuestAuction auction;

    public Guest(GuestAuction auction) {
        this.auction = auction;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean isAuthorized() {
        return false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Bet> getAllBets() {
        return auction.getBets();
    }

    @Override
    public List<Lot> getAllLots() {
        return auction.getLots();
    }

    @Override
    public List<Lot> getLotsForCategory(String category) {
        List<Lot> lots = new ArrayList<>();
        lots.addAll(auction.getLotsForCategory(category));
        return lots;
    }
}
