package com.egarcourse.auction.domain.users;

import com.egarcourse.auction.domain.users.cliententity.Bet;
import com.egarcourse.auction.domain.users.cliententity.Lot;

import java.util.List;

/**
 * Auction's user interface, provides basics operation, that valid on all users
 */
public interface User {
    boolean isAuthorized();
    String getName();

    /**
     * Returns all active bets from auction
     * @return bets
     */
    List<Bet> getAllBets();
    /**
     * Returns all active lots from auction
     * @return lots
     */
    List<Lot> getAllLots();

    /**
     * Returns all active lots from category
     * @param category
     * @return lots
     */
    List<Lot> getLotsForCategory(String category);
}
