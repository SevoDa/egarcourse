package com.egarcourse.auction.services;

import com.egarcourse.auction.datamodel.DataAccessEnvironment;
import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.AdministratorDTO;
import com.egarcourse.auction.datamodel.entities.ClientDTO;
import com.egarcourse.auction.datamodel.entities.LotDTO;
import com.egarcourse.auction.domain.AdministratorAuction;
import com.egarcourse.auction.domain.ClientAuction;
import com.egarcourse.auction.domain.users.Administrator;
import com.egarcourse.auction.domain.users.Client;
import com.egarcourse.auction.domain.users.cliententity.Lot;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayDeque;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.logging.Logger;

/**
 * Service, provides authorization
 */
public class AuthorizationService {
    private static final Logger logger = Logger.getLogger(AuthorizationService.class.getName());

    private static final int LIMIT_CASHES_SIZE = 1500;

    private HashMap<String, ClientDTO> clientsCash;
    private HashMap<String, AdministratorDTO> administratorsCash;
    private DataAccessEnvironment environment;

    public void init(DataAccessEnvironment environment) {
        clientsCash = new HashMap<>();
        administratorsCash = new HashMap<>();
        this.environment = environment;
    }

    public boolean containsClientLogin(String login) {
        if(!clientsCash.containsKey(login)) {
            DataAccess<ClientDTO> dao = environment.getClientDAO();
            ClientDTO dto = dao.select(login);
            if(dto == null) return false;
        }
        return true;
    }

    public boolean containsAdminLogin(String login) {
        if(!administratorsCash.containsKey(login)) {
            DataAccess<AdministratorDTO> dao = environment.getAdminDAO();
            AdministratorDTO dto = dao.select(login);
            if(dto == null) return false;
        }
        return true;
    }

    public Client authorizeClient(ClientAuction auction, String login, String password){
        if(!tryAuthorizeClient(login, password)) {
            logger.warning("Should call tryAuthorizeClient() before authorizeClient()");
            throw new IllegalArgumentException("Wrong login or password");
        }
        Client client = new Client(auction);
        ClientDTO dto = clientsCash.get(login);
        client.setLogin(dto.getLogin());
        client.setId(dto.getAccountId());
        client.setMoney(dto.getMoney());
        client.setName(dto.getName());

        DataAccess<LotDTO> lotDTODataAccess = environment.getLotsDAO();
        for(long id : dto.getLots()) {
            LotDTO lotDTO = lotDTODataAccess.select(id);
            Lot lot = new Lot();
            lot.setCategory(lotDTO.getCategory());
            lot.setLotId(lotDTO.getLotId());
            lot.setName(lotDTO.getName());
            lot.setActive(false);
            lot.setCreationTime(lotDTO.getCreationTime());
            lot.setClosingTime(lotDTO.getClosingTime());
            lot.setBetsStack(new ArrayDeque<>());

            client.addLot(lot);
        }

        client.setAuthorized(true);

        clientsCash.remove(login);
        return client;
    }

    public Administrator authorizeAdministrator(AdministratorAuction auction, String login, String password){
        if(!tryAuthorizeAdministrator(login, password)) {
            logger.warning("Should call tryAuthorizeAdministrator() before authorizeAdministrator()");
            throw new IllegalArgumentException("Wrong login or password");
        }
        Administrator admin = new Administrator(auction);
        AdministratorDTO dto = administratorsCash.get(login);
        admin.setId(dto.getAccountId());
        admin.setLogin(dto.getLogin());
        admin.setName(dto.getName());

        admin.setAuthorized(true);

        administratorsCash.remove(login);
        return admin;
    }

    /**
     * Tries to authorization as client
     * You should call authorizeClient after that to get Client entity
     * @param login
     * @param password
     * @return
     */
    public boolean tryAuthorizeClient(String login, String password) {
        if(!containsClientLogin(login)) return false;

        if(!clientsCash.containsKey(login)) {
            DataAccess<ClientDTO> dao = environment.getClientDAO();
            ClientDTO dto = dao.select(login);
            if(dto != null) clientsCash.put(login, dto);
        }

        ClientDTO dto = clientsCash.get(login);

        if(clientsCash.keySet().size() + administratorsCash.keySet().size() > LIMIT_CASHES_SIZE) {
            cleanCashes();
        }

        return tryCashedAuthorize(login, password, dto.getPasswordHash());
    }

    /**
     * Tries to authorization as administrator
     * You should call authorizeAdministrator after that to get Administrator entity
     * @param login
     * @param password
     * @return
     */
    public boolean tryAuthorizeAdministrator(String login, String password) {
        if(!containsAdminLogin(login)) return false;

        if(!administratorsCash.containsKey(login)) {
            DataAccess<AdministratorDTO> dao = environment.getAdminDAO();
            AdministratorDTO dto = dao.select(login);
            if(dto != null) administratorsCash.put(login, dto);
        }

        AdministratorDTO dto = administratorsCash.get(login);

        if(clientsCash.keySet().size() + administratorsCash.keySet().size() > LIMIT_CASHES_SIZE) {
            cleanCashes();
        }

        return tryCashedAuthorize(login, password, dto.getPasswordHash());
    }

    /**
     * Code from:
     * stackoverflow.com/questions/5531455/how-to-hash-some-string-with-sha256-in-java
     * @param base
     * @return hash from string
     */
    public String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            logger.severe(e.getMessage() + "\n");
            throw new RuntimeException(e);
        }
    }

    private void cleanCashes() {
        clientsCash.clear();
        administratorsCash.clear();
    }

    private boolean tryCashedAuthorize(String login, String password, String passHashCash) {
        String inputPassHash = sha256(password);
        if(passHashCash.equals(inputPassHash)) {
            return true;
        }
        return false;
    }
}
