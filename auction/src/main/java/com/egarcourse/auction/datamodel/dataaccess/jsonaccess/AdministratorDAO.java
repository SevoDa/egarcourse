package com.egarcourse.auction.datamodel.dataaccess.jsonaccess;

import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.AdministratorDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.deploy.util.StringUtils;

import java.io.*;
import java.lang.reflect.Type;
import java.util.*;
import java.util.logging.Logger;

/**
 * Data Access Object
 * Provides CRUD operations, works with AdministratorDTO
 */
public class AdministratorDAO implements DataAccess<AdministratorDTO> {
    private static final Logger logger = Logger.getLogger(AdministratorDAO.class.getName());

    private static final Type type = new TypeToken<ArrayList<AdministratorDTO>>() {}.getType();

    private File adminsFile;
    private Gson gson;

    public void init(String... adminsFileName) {
        if(adminsFileName.length != 1) throw new IllegalArgumentException("json init consume 1 argument");

        try {
            adminsFile = new File(adminsFileName[0]);

            if (!adminsFile.exists()) {
                adminsFile.createNewFile();
            }

            gson = new Gson();

        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public int insert(AdministratorDTO dto) {
        try{
            List<AdministratorDTO> all;

            try(FileReader reader = new FileReader(adminsFile)) {
                all = gson.fromJson(reader, type);
            }

            if (all == null) {
                all = new ArrayList<>();
            }

            all.sort((x,y) -> Long.compare(x.getAccountId(), y.getAccountId()));
            int ind = Collections.binarySearch(all, dto, (x,y) -> Long.compare(x.getAccountId(), y.getAccountId()));


            if(ind >= 0 && ind < all.size()) {
                return 0;
            }

            if(all.size() == 0) dto.setAccountId(0);
            else dto.setAccountId(all.get(all.size()-1).getAccountId()+1);
            all.add(dto);

            String outputJson = gson.toJson(all);

            try(FileWriter writer = new FileWriter(adminsFile)) {
                writer.write(outputJson);
            }

            return 1;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public AdministratorDTO select(Object loginObj) {
        String login = (String) loginObj;

        try {
            List<AdministratorDTO> all;

            try (FileReader reader = new FileReader(adminsFile)){
               all = gson.fromJson(reader, type);
            }

            if(all == null) all = new ArrayList<>();

            AdministratorDTO dto = new AdministratorDTO();
            dto.setLogin(login);

            all.sort((x,y) -> x.getLogin().compareTo(y.getLogin()));
            int ind = Collections.binarySearch(all, dto, (x,y) -> x.getLogin().compareTo(y.getLogin()));
            if(ind < 0 || ind >= all.size()) return null;

            dto = all.get(ind);

            return dto;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public List<AdministratorDTO> selectAll() {
        try (FileReader reader = new FileReader(adminsFile)){
            List<AdministratorDTO> all = gson.fromJson(reader, type);
            if(all == null) all = new ArrayList<>();
            all.sort((x,y) -> x.getLogin().compareTo(y.getLogin()));

            return all;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public int update(AdministratorDTO dto) {
        try {
            List<AdministratorDTO> all;
            try (FileReader reader = new FileReader(adminsFile)){
                all = gson.fromJson(reader, type);
            }
            if(all == null) all = new ArrayList<>();

            all.sort((x,y) -> Long.compare(x.getAccountId(), y.getAccountId()));
            int ind = Collections.binarySearch(all, dto, (x,y) -> Long.compare(x.getAccountId(), y.getAccountId()));

            if(ind < 0 || ind >= all.size() || all.get(ind).getAccountId() != dto.getAccountId()) return 0;

            all.set(ind, dto);

            String outputJson = gson.toJson(all);
            try(FileWriter writer = new FileWriter(adminsFile)) {
                writer.write(outputJson);
            }
            return 1;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public int delete(AdministratorDTO dto) {
        try{
            List<AdministratorDTO> all;

            try(FileReader reader = new FileReader(adminsFile)){
                all = gson.fromJson(reader, type);
            }

            if(all == null) all = new ArrayList<>();

            all.sort((x,y) -> Long.compare(x.getAccountId(), y.getAccountId()));
            int ind = Collections.binarySearch(all, dto, (x,y) -> Long.compare(x.getAccountId(), y.getAccountId()));

            if(ind < 0 || ind >= all.size() || all.get(ind).getAccountId() != dto.getAccountId()) return 0;

            dto = all.get(ind);

            all.remove(dto);

            String outputJson = gson.toJson(all);

            try(FileWriter writer = new FileWriter(adminsFile)) {
                writer.write(outputJson);
            }
            return 1;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }
}
