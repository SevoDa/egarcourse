package com.egarcourse.auction.datamodel.dataaccess.sqlaccess;

import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.ClientDTO;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Data Access Object
 * Provides CRUD operations, works with ClientDTO
 */
public class ClientDAO implements DataAccess<ClientDTO> {
    private static final Logger logger = Logger.getLogger(AdministratorDAO.class.getName());

    private static final String ACCOUNT_ID_COLUMN = "account_id";
    private static final String LOGIN_COLUMN = "login";
    private static final String NAME_COLUMN = "name";
    private static final String PASSWORD_HASH_COLUMN = "password_hash";
    private static final String MONEY_COLUMN = "money";
    private static final String LOTS_COLUMN = "lots";
    private static final String BIRTHDAY_COLUMN = "birthday";

    private DataSource dataSource;
    private String dbUserName;
    private String dbUserPassword;
    private String dbJndiName;
    private String clientTableName;

    public void init(String... args) {
        if(args.length <= 4) throw new IllegalArgumentException("sql init consume 4 arguments");

        dbUserName = args[0];
        dbUserPassword = args[1];
        this.dbJndiName = args[2];
        this.clientTableName = args[3];

        Context ctx = null;

        try {
            ctx = new InitialContext();
            dataSource = (DataSource)ctx.lookup(this.dbJndiName);
        } catch (NamingException e) {
            logger.severe("Context was not founded");
            e.printStackTrace();
        }
    }

    public int insert(ClientDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("INSERT INTO ");
        queryBuilder.append(clientTableName);
        queryBuilder.append("(" +
                ACCOUNT_ID_COLUMN + ", " +
                LOGIN_COLUMN + ", " +
                NAME_COLUMN + ", " +
                PASSWORD_HASH_COLUMN + ", " +
                MONEY_COLUMN + ", " +
                LOTS_COLUMN + ", " +
                BIRTHDAY_COLUMN + ", " +
                ") ");
        queryBuilder.append("VALUES (");
        queryBuilder.append(String.format("%s ,", dto.getAccountId()));
        queryBuilder.append(String.format("%s ,", dto.getLogin()));
        queryBuilder.append(String.format("%s ,", dto.getName()));
        queryBuilder.append(String.format("%s ,", dto.getPasswordHash()));
        queryBuilder.append(String.format("%s ,", dto.getMoney()));
        queryBuilder.append(String.format("%s ,", dto.getLots()));
        queryBuilder.append(String.format("%s ,", dto.getBirthDay().toString()));
        queryBuilder.append(");");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement()
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public ClientDTO select(Object loginObj) {
        String login = (String)loginObj;

        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(clientTableName);
        queryBuilder.append(" WHERE ");
        queryBuilder.append(LOGIN_COLUMN + " = ");
        queryBuilder.append(login);
        queryBuilder.append(";");

        ClientDTO dto = new ClientDTO();

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(queryBuilder.toString())
        ) {

            dto.setAccountId(resultSet.getLong(0));
            dto.setLogin(resultSet.getString(1));
            dto.setName(resultSet.getString(2));
            dto.setPasswordHash(resultSet.getString(3));
            dto.setMoney(resultSet.getLong(4));
            dto.setLots((long[])resultSet.getArray(5).getArray());
            dto.setBirthDay(Date.valueOf(resultSet.getString(6)));

            return dto;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ClientDTO> selectAll() {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(clientTableName);
        queryBuilder.append(";");



        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(queryBuilder.toString())
        ) {
            List<ClientDTO> dtoList = new ArrayList<>();
            while(resultSet.next()) {
                ClientDTO dto = new ClientDTO();
                dto.setAccountId(resultSet.getLong(0));
                dto.setLogin(resultSet.getString(1));
                dto.setName(resultSet.getString(2));
                dto.setPasswordHash(resultSet.getString(3));
                dto.setMoney(resultSet.getLong(4));
                dto.setLots((long[])resultSet.getArray(5).getArray());
                dto.setBirthDay(Date.valueOf(resultSet.getString(6)));
                dtoList.add(dto);
            }

            return dtoList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int update(ClientDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("UPDATE ");
        queryBuilder.append(clientTableName);
        queryBuilder.append(" SET ");
        queryBuilder.append(NAME_COLUMN + " = ");
        queryBuilder.append(dto.getName() + ", ");
        queryBuilder.append(PASSWORD_HASH_COLUMN + " = ");
        queryBuilder.append(dto.getPasswordHash() + ", ");
        queryBuilder.append(MONEY_COLUMN + " = ");
        queryBuilder.append(dto.getMoney() + ", ");
        queryBuilder.append(LOTS_COLUMN + " = ");
        queryBuilder.append(dto.getLots() + ", ");
        queryBuilder.append(BIRTHDAY_COLUMN + " = ");
        queryBuilder.append(dto.getBirthDay().toString());
        queryBuilder.append(" WHERE ");
        queryBuilder.append(ACCOUNT_ID_COLUMN + " = ");
        queryBuilder.append(dto.getAccountId() + " AND ");
        queryBuilder.append(LOGIN_COLUMN + " = ");
        queryBuilder.append(dto.getLogin());
        queryBuilder.append(";");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int delete(ClientDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("DELETE FROM ");
        queryBuilder.append(clientTableName);
        queryBuilder.append("WHERE ");
        queryBuilder.append(LOGIN_COLUMN + " = ");
        queryBuilder.append(dto.getLogin() + " AND ");
        queryBuilder.append(ACCOUNT_ID_COLUMN + " = ");
        queryBuilder.append(dto.getAccountId());
        queryBuilder.append(";");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement()
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
