package com.egarcourse.auction.datamodel;

import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.AdministratorDTO;
import com.egarcourse.auction.datamodel.entities.BetDTO;
import com.egarcourse.auction.datamodel.entities.ClientDTO;
import com.egarcourse.auction.datamodel.entities.LotDTO;

public interface DataAccessEnvironment {
    void init();

    DataAccess<ClientDTO> getClientDAO();

    DataAccess<AdministratorDTO> getAdminDAO();

    DataAccess<LotDTO> getLotsDAO();

    DataAccess<BetDTO> getBetsDAO();
}
