package com.egarcourse.auction.datamodel.dataaccess.jsonaccess;

import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.BetDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * Data Access Object
 * Provides CRUD operations, works with BetDTO
 */
public class BetDAO implements DataAccess<BetDTO> {
    private static final Logger logger = Logger.getLogger(BetDAO.class.getName());

    private static final Type type = new TypeToken<ArrayList<BetDTO>>() {}.getType();

    private File betsFile;
    private Gson gson;

    public void init(String... betsFileName) {
        if(betsFileName.length != 1) throw new IllegalArgumentException("json init consume 1 argument");

        try {
            betsFile = new File(betsFileName[0]);

            if(!betsFile.exists()) {
                betsFile.createNewFile();
            }

            gson = new Gson();

        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public int insert(BetDTO dto) {
        try{
            List<BetDTO> all;

            try(FileReader reader = new FileReader(betsFile)) {
                all = gson.fromJson(reader, type);
            }

            if (all == null) {
                all = new ArrayList<>();
            }

            all.sort((x,y) -> Long.compare(x.getBetId(), y.getBetId()));
            int ind = Collections.binarySearch(all, dto, (x, y) -> Long.compare(x.getBetId(), y.getBetId()));


            if(ind >= 0 && ind < all.size()) {
                return 0;
            }

            if(all.size() == 0) dto.setBetId(0);
            else dto.setBetId(all.get(all.size()-1).getBetId()+1);
            all.add(dto);

            String outputJson = gson.toJson(all);

            try(FileWriter writer = new FileWriter(betsFile)) {
                writer.write(outputJson);
            }

            return 1;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public BetDTO select(Object betIdObj) {
        Long betId = (Long) betIdObj;

        try (FileReader reader = new FileReader(betsFile)){
            List<BetDTO> all = gson.fromJson(reader, type);

            if(all == null) all = new ArrayList<>();

            BetDTO dto = new BetDTO();
            dto.setBetId(betId);

            all.sort((x,y) -> Long.compare(x.getBetId(),y.getBetId()));
            int ind = Collections.binarySearch(all, dto, (x,y) -> Long.compare(x.getBetId(),y.getBetId()));
            if(ind < 0 || ind >= all.size()) return null;

            dto = all.get(ind);

            return dto;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public List<BetDTO> selectAll() {
        try (FileReader reader = new FileReader(betsFile)){
            List<BetDTO> all = gson.fromJson(reader, type);
            if(all == null) all = new ArrayList<>();
            all.sort((x,y) -> Long.compare(x.getBetId(),y.getBetId()));

            return all;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public int update(BetDTO dto) {
        try {
            List<BetDTO> all;
            try (FileReader reader = new FileReader(betsFile)){
                all = gson.fromJson(reader, type);
            }
            if(all == null) all = new ArrayList<>();

            all.sort((x,y) -> Long.compare(x.getBetId(), y.getBetId()));
            int ind = Collections.binarySearch(all, dto, (x,y) -> Long.compare(x.getBetId(),y.getBetId()));

            if(ind < 0 || ind >= all.size() || all.get(ind).getBetId() != dto.getBetId()) return 0;

            all.set(ind, dto);

            String outputJson = gson.toJson(all);
            try(FileWriter writer = new FileWriter(betsFile)) {
                writer.write(outputJson);
            }
            return 1;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public int delete(BetDTO dto) {
        try {
            List<BetDTO> all;
            try(FileReader reader = new FileReader(betsFile)){
                all = gson.fromJson(reader, type);
            }

            if(all == null) all = new ArrayList<>();

            all.sort((x,y) -> Long.compare(x.getBetId(), y.getBetId()));
            int ind = Collections.binarySearch(all, dto, (x, y) -> Long.compare(x.getBetId(), y.getBetId()));

            if(ind < 0 || ind >= all.size() || all.get(ind).getBetId() != dto.getBetId()) return 0;

            dto = all.get(ind);

            all.remove(dto);

            String outputJson = gson.toJson(all);
            try(FileWriter writer = new FileWriter(betsFile)) {
                writer.write(outputJson);
            }
            return 1;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }
}
