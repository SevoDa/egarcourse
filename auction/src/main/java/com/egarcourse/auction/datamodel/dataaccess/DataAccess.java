package com.egarcourse.auction.datamodel.dataaccess;

import java.util.List;

public interface DataAccess<T> {
    void init(String... args);
    int insert(T dto);
    T select(Object key);
    List<T> selectAll();
    int update(T dto);
    int delete(T dto);
}
