package com.egarcourse.auction.datamodel.entities;

import java.io.Serializable;
import java.util.Date;

public class ClientDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private long accountId;
    private String name;
    private String login;
    private String passwordHash;
    private long money;
    private long[] lots;
    private Date birthDay;

    public ClientDTO() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public long[] getLots() {
        return lots;
    }

    public void setLots(long[] lots) {
        this.lots = lots;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }
}
