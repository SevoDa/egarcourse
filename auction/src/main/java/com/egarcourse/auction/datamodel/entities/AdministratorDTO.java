package com.egarcourse.auction.datamodel.entities;

import java.io.Serializable;

public class AdministratorDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private long accountId;
    private String name;
    private String login;
    private String passwordHash;

    public AdministratorDTO() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }
}
