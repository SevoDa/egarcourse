package com.egarcourse.auction.datamodel.dataaccess.sqlaccess;

import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.LotDTO;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Data Access Object
 * Provides CRUD operations, works with LotDTO
 */
public class LotDAO implements DataAccess<LotDTO> {
    private static final Logger logger = Logger.getLogger(AdministratorDAO.class.getName());

    private static final String LOT_ID_COLUMN =  "lot_id";
    private static final String CATEGORY_COLUMN = "category";
    private static final String NAME_COLUMN = "name";
    private static final String OWNER_ID_COLUMN = "owner_id";
    private static final String CREATION_TIME_COLUMN = "creation_time";
    private static final String CLOSING_TIME_COLUMN = "closing_time";

    private DataSource dataSource;
    private String dbUserName;
    private String dbUserPassword;
    private String dbJndiName;
    private String lotTableName;

    public void init(String... args) {
        if(args.length <= 4) throw new IllegalArgumentException("sql init consume 4 arguments");
        dbUserName = args[0];
        dbUserPassword = args[1];
        this.dbJndiName = args[2];
        this.lotTableName = args[3];

        Context ctx = null;

        try {
            ctx = new InitialContext();
            dataSource = (DataSource)ctx.lookup(dbJndiName);
        } catch (NamingException e) {
            logger.severe("Context was not founded");
            e.printStackTrace();
        }
    }

    public int insert(LotDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("INSERT INTO ");
        queryBuilder.append(lotTableName);
        queryBuilder.append("(" +
                LOT_ID_COLUMN + ", " +
                CATEGORY_COLUMN + ", " +
                NAME_COLUMN + ", " +
                OWNER_ID_COLUMN + ", " +
                CREATION_TIME_COLUMN + ", " +
                CLOSING_TIME_COLUMN + ", " +
                ") ");
        queryBuilder.append("VALUES (");
        queryBuilder.append(String.format("%s ,", dto.getLotId()));
        queryBuilder.append(String.format("%s ,", dto.getCategory()));
        queryBuilder.append(String.format("%s ,", dto.getName()));
        queryBuilder.append(String.format("%s ,", dto.getOwnerId()));
        queryBuilder.append(String.format("%s ,", dto.getCreationTime()));
        queryBuilder.append(String.format("%s ,", dto.getClosingTime()));
        queryBuilder.append(");");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement()
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public LotDTO select(Object lotId) {
        Long id = (Long) lotId;
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(lotTableName);
        queryBuilder.append(" WHERE ");
        queryBuilder.append(LOT_ID_COLUMN + " = ");
        queryBuilder.append(id);
        queryBuilder.append(";");

        LotDTO dto = new LotDTO();

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(queryBuilder.toString())
        ) {

            dto.setLotId(resultSet.getLong(0));
            dto.setCategory(resultSet.getString(1));
            dto.setName(resultSet.getString(2));
            dto.setOwnerId(resultSet.getLong(3));
            dto.setCreationTime(Date.valueOf(resultSet.getString(4)));
            dto.setClosingTime(Date.valueOf(resultSet.getString(5)));

            return dto;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<LotDTO> selectAll() {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(lotTableName);
        queryBuilder.append(";");


        try (Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(queryBuilder.toString())
        ) {
            List<LotDTO> dtoList = new ArrayList<>();
            while (resultSet.next()) {
                LotDTO dto = new LotDTO();
                dto.setLotId(resultSet.getLong(0));
                dto.setCategory(resultSet.getString(1));
                dto.setName(resultSet.getString(2));
                dto.setOwnerId(resultSet.getLong(3));
                dto.setCreationTime(Date.valueOf(resultSet.getString(4)));
                dto.setClosingTime(Date.valueOf(resultSet.getString(5)));
                dtoList.add(dto);
            }

            return dtoList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int update(LotDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("UPDATE ");
        queryBuilder.append(lotTableName);
        queryBuilder.append(" SET ");
        queryBuilder.append(NAME_COLUMN + " = ");
        queryBuilder.append(dto.getName() + ", ");
        queryBuilder.append(OWNER_ID_COLUMN + " = ");
        queryBuilder.append(dto.getOwnerId() + ", ");
        queryBuilder.append(CREATION_TIME_COLUMN + " = ");
        queryBuilder.append(dto.getCreationTime() +  ", ");
        queryBuilder.append(CLOSING_TIME_COLUMN + " = ");
        queryBuilder.append(dto.getClosingTime());
        queryBuilder.append(" WHERE ");
        queryBuilder.append(LOT_ID_COLUMN + " = ");
        queryBuilder.append(dto.getLotId());
        queryBuilder.append(";");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }


    public int delete(LotDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("DELETE FROM ");
        queryBuilder.append(lotTableName);
        queryBuilder.append("WHERE ");
        queryBuilder.append(LOT_ID_COLUMN + " = ");
        queryBuilder.append(dto.getLotId() + " AND ");
        queryBuilder.append(OWNER_ID_COLUMN + " = ");
        queryBuilder.append(dto.getOwnerId());
        queryBuilder.append(";");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement()
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
