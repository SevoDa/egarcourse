package com.egarcourse.auction.datamodel;

import com.egarcourse.auction.datamodel.dataaccess.*;
import com.egarcourse.auction.datamodel.dataaccess.sqlaccess.AdministratorDAO;
import com.egarcourse.auction.datamodel.dataaccess.sqlaccess.BetDAO;
import com.egarcourse.auction.datamodel.dataaccess.sqlaccess.ClientDAO;
import com.egarcourse.auction.datamodel.dataaccess.sqlaccess.LotDAO;
import com.egarcourse.auction.datamodel.entities.AdministratorDTO;
import com.egarcourse.auction.datamodel.entities.BetDTO;
import com.egarcourse.auction.datamodel.entities.ClientDTO;
import com.egarcourse.auction.datamodel.entities.LotDTO;

public class PostgresEnvironment implements DataAccessEnvironment {
    private static final String dbLocation = "jdbc:postgresql://egarauction/database";
    private static final String userName = "admin";
    private static final String userPassword = "admin";

    private DataAccess<ClientDTO> clientDAO = new ClientDAO();
    private DataAccess<AdministratorDTO> adminDAO = new AdministratorDAO();
    private DataAccess<LotDTO> lotsDAO = new LotDAO();
    private DataAccess<BetDTO> betsDAO = new BetDAO();

    public void init() {
        clientDAO.init(userName, userPassword, dbLocation, "clients");
        adminDAO.init(userName, userPassword, dbLocation, "admins");
        lotsDAO.init(userName, userPassword, dbLocation, "lots");
        betsDAO.init(userName, userPassword, dbLocation, "bets");
    }

    public DataAccess<ClientDTO> getClientDAO() {
        return clientDAO;
    }

    public DataAccess<AdministratorDTO> getAdminDAO() {
        return adminDAO;
    }

    public DataAccess<LotDTO> getLotsDAO() {
        return lotsDAO;
    }

    public DataAccess<BetDTO> getBetsDAO() {
        return betsDAO;
    }
}
