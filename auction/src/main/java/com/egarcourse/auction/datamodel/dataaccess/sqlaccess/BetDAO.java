package com.egarcourse.auction.datamodel.dataaccess.sqlaccess;

import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.BetDTO;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Data Access Object
 * Provides CRUD operations, works with BetDTO
 */
public class BetDAO implements DataAccess<BetDTO> {
    private static final Logger logger = Logger.getLogger(BetDAO.class.getName());

    private static final String BET_ID_COLUMN = "bet_id";
    private static final String LOT_ID_COLUMN = "lot_id";
    private static final String OWNER_ID_COLUMN = "owner_id";
    private static final String PRICE_COLUMN = "price";
    private static final String CREATING_TIME_COLUMN = "creation_time";
    private static final String STATUS_COLUMN = "status";

    private DataSource dataSource;
    private String dbUserName;
    private String dbUserPassword;
    private String dbJndiName;
    private String betTableName;

    public void init(String... args) {
        if(args.length <= 4) throw new IllegalArgumentException("sql init consume 4 arguments");

        dbUserName = args[0];
        dbUserPassword = args[1];
        this.dbJndiName = args[2];
        this.betTableName = args[3];

        Context ctx = null;

        try {
            ctx = new InitialContext();
            dataSource = (DataSource)ctx.lookup(this.dbJndiName);
        } catch (NamingException e) {
            logger.severe("Context was not founded");
            e.printStackTrace();
        }
    }

    public int insert(BetDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("INSERT INTO ");
        queryBuilder.append(betTableName);
        queryBuilder.append("(" +
                BET_ID_COLUMN + ", " +
                LOT_ID_COLUMN + ", " +
                OWNER_ID_COLUMN + ", " +
                PRICE_COLUMN + ", " +
                CREATING_TIME_COLUMN + ", " +
                STATUS_COLUMN + ", " +
                ") ");
        queryBuilder.append("VALUES (");
        queryBuilder.append(String.format("%s ,", dto.getBetId()));
        queryBuilder.append(String.format("%s ,", dto.getLotId()));
        queryBuilder.append(String.format("%s ,", dto.getOwnerId()));
        queryBuilder.append(String.format("%s ,", dto.getPrice()));
        queryBuilder.append(String.format("%s ,", dto.getCreationTime().toString()));
        queryBuilder.append(String.format("%s ,", dto.isCommitted()));
        queryBuilder.append(");");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement()
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public BetDTO select(Object betId) {
        Long id = (Long) betId;

        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(betTableName);
        queryBuilder.append(" WHERE ");
        queryBuilder.append(BET_ID_COLUMN + " = ");
        queryBuilder.append(id);
        queryBuilder.append(";");

        BetDTO dto = new BetDTO();

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(queryBuilder.toString())
        ) {

            dto.setBetId(resultSet.getLong(0));
            dto.setLotId(resultSet.getLong(1));
            dto.setOwnerId(resultSet.getLong(2));
            dto.setPrice(resultSet.getLong(3));
            dto.setCreationTime(Date.valueOf(resultSet.getString(4)));
            dto.setCommitted(resultSet.getBoolean(5));

            return dto;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<BetDTO> selectAll() {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(betTableName);
        queryBuilder.append(";");


        try (Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(queryBuilder.toString())
        ) {
            List<BetDTO> dtoList = new ArrayList<>();
            while (resultSet.next()) {
                BetDTO dto = new BetDTO();
                dto.setBetId(resultSet.getLong(0));
                dto.setLotId(resultSet.getLong(1));
                dto.setOwnerId(resultSet.getLong(2));
                dto.setPrice(resultSet.getLong(3));
                dto.setCreationTime(Date.valueOf(resultSet.getString(4)));
                dto.setCommitted(resultSet.getBoolean(5));
                dtoList.add(dto);
            }

            return dtoList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int update(BetDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("UPDATE ");
        queryBuilder.append(betTableName);
        queryBuilder.append(" SET ");
        queryBuilder.append(LOT_ID_COLUMN + " = ");
        queryBuilder.append(dto.getLotId() + ", ");
        queryBuilder.append(OWNER_ID_COLUMN + " = ");
        queryBuilder.append(dto.getOwnerId() + ", ");
        queryBuilder.append(PRICE_COLUMN + " = ");
        queryBuilder.append(dto.getPrice() + ", ");
        queryBuilder.append(CREATING_TIME_COLUMN + " = ");
        queryBuilder.append(dto.getCreationTime() + ", ");
        queryBuilder.append(STATUS_COLUMN + " = ");
        queryBuilder.append(dto.isCommitted());
        queryBuilder.append(" WHERE ");
        queryBuilder.append(BET_ID_COLUMN + " = ");
        queryBuilder.append(dto.getBetId() + " AND ");
        queryBuilder.append(";");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int delete(BetDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("DELETE FROM ");
        queryBuilder.append(betTableName);
        queryBuilder.append("WHERE ");
        queryBuilder.append(BET_ID_COLUMN + " = ");
        queryBuilder.append(dto.getBetId() + " AND ");
        queryBuilder.append(LOT_ID_COLUMN + " = ");
        queryBuilder.append(dto.getLotId() + " AND ");
        queryBuilder.append(OWNER_ID_COLUMN + " = ");
        queryBuilder.append(dto.getOwnerId() + " AND ");
        queryBuilder.append(PRICE_COLUMN + " = ");
        queryBuilder.append(dto.getPrice());
        queryBuilder.append(";");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement()
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
