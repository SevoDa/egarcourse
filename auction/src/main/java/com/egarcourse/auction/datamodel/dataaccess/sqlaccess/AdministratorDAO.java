package com.egarcourse.auction.datamodel.dataaccess.sqlaccess;

import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.AdministratorDTO;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Data Access Object
 * Provides CRUD operations, works with AdministratorDTO
 */
public class AdministratorDAO implements DataAccess<AdministratorDTO> {
    private static final Logger logger = Logger.getLogger(AdministratorDAO.class.getName());

    private static final String ACC_ID_COLUMN = "account_id";
    private static final String NAME_COLUMN = "name";
    private static final String LOGIN_COLUMN = "login";
    private static final String PASS_HASH_COLUMN = "pass_hash";

    private DataSource dataSource;
    private String dbUserName;
    private String dbUserPassword;
    private String dbJndiName;
    private String adminTableName;

    public void init(String... args) {
        if(args.length <= 4) throw new IllegalArgumentException("sql init consume 4 arguments");

        dbUserName = args[0];
        dbUserPassword = args[1];
        this.dbJndiName = args[2];
        this.adminTableName = args[3];

        Context ctx;

        try {
            ctx = new InitialContext();
            dataSource = (DataSource)ctx.lookup(this.dbJndiName);
        } catch (NamingException e) {
            logger.severe("Context was not founded");
            e.printStackTrace();
        }
    }

    public int insert(AdministratorDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("INSERT INTO ");
        queryBuilder.append(adminTableName);
        queryBuilder.append("(" +
                ACC_ID_COLUMN + ", " +
                NAME_COLUMN + ", " +
                LOGIN_COLUMN + ", " +
                PASS_HASH_COLUMN +
                ") ");
        queryBuilder.append("VALUES (");
        queryBuilder.append(String.format("%s ,", dto.getAccountId()));
        queryBuilder.append(String.format("%s ,", dto.getName()));
        queryBuilder.append(String.format("%s ,", dto.getLogin()));
        queryBuilder.append(String.format("%s ,", dto.getPasswordHash()));
        queryBuilder.append(");");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement()
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public AdministratorDTO select(Object loginObj) {
        String login = (String) loginObj;

        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(adminTableName);
        queryBuilder.append(" WHERE ");
        queryBuilder.append(LOGIN_COLUMN + " = ");
        queryBuilder.append(login);
        queryBuilder.append(";");

        AdministratorDTO dto = new AdministratorDTO();

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(queryBuilder.toString())
        ) {

            dto.setAccountId(resultSet.getInt(0));
            dto.setName(resultSet.getString(1));
            dto.setLogin(resultSet.getString(2));
            dto.setPasswordHash(resultSet.getString(3));

            return dto;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<AdministratorDTO> selectAll() {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(adminTableName);
        queryBuilder.append(";");



        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(queryBuilder.toString())
        ) {
            List<AdministratorDTO> dtoList = new ArrayList<>();
            while(resultSet.next()) {
                AdministratorDTO dto = new AdministratorDTO();
                dto.setAccountId(resultSet.getInt(0));
                dto.setName(resultSet.getString(1));
                dto.setLogin(resultSet.getString(2));
                dto.setPasswordHash(resultSet.getString(3));
                dtoList.add(dto);
            }

            return dtoList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int update(AdministratorDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("UPDATE ");
        queryBuilder.append(adminTableName);
        queryBuilder.append(" SET ");
        queryBuilder.append(NAME_COLUMN +" = ");
        queryBuilder.append(dto.getName() + ", ");
        queryBuilder.append(PASS_HASH_COLUMN + " = ");
        queryBuilder.append(dto.getPasswordHash());
        queryBuilder.append(" WHERE ");
        queryBuilder.append(LOGIN_COLUMN + " = ");
        queryBuilder.append(dto.getLogin() + " AND ");
        queryBuilder.append(ACC_ID_COLUMN + " = ");
        queryBuilder.append(dto.getAccountId());
        queryBuilder.append(";");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement();
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int delete(AdministratorDTO dto) {
        StringBuilder queryBuilder = new StringBuilder();

        // TODO: ? Can SQL injections exists here ?

        queryBuilder.append("DELETE FROM ");
        queryBuilder.append(adminTableName);
        queryBuilder.append("WHERE ");
        queryBuilder.append(ACC_ID_COLUMN + " = ");
        queryBuilder.append(dto.getAccountId() + " AND");
        queryBuilder.append(LOGIN_COLUMN + " = ");
        queryBuilder.append(dto.getLogin());
        queryBuilder.append(";");

        try(Connection connection = dataSource.getConnection(dbUserName, dbUserPassword);
            Statement statement = connection.createStatement()
        ) {
            return statement.executeUpdate(queryBuilder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
