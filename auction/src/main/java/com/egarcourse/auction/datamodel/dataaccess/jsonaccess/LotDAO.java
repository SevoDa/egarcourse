package com.egarcourse.auction.datamodel.dataaccess.jsonaccess;

import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.entities.LotDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * Data Access Object
 * Provides CRUD operations, works with LotDTO
 */
public class LotDAO implements DataAccess<LotDTO> {
    private static final Logger logger = Logger.getLogger(AdministratorDAO.class.getName());

    private static final Type type = new TypeToken<ArrayList<LotDTO>>() {}.getType();

    private File lotsFile;
    private Gson gson;

    public void init(String... lotsFileName) {
        if(lotsFileName.length != 1) throw new IllegalArgumentException("json init consume 1 argument");

        try {
            lotsFile = new File(lotsFileName[0]);

            if(!lotsFile.exists()) {
                lotsFile.createNewFile();
            }

            gson = new Gson();

        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public int insert(LotDTO dto) {
        try{
            List<LotDTO> all;

            try(FileReader reader = new FileReader(lotsFile)) {
                all = gson.fromJson(reader, type);
            }

            if (all == null) {
                all = new ArrayList<>();
            }


            all.sort((x,y) -> Long.compare(x.getLotId(), y.getLotId()));
            int ind = Collections.binarySearch(all, dto, (x, y) -> Long.compare(x.getLotId(), y.getLotId()));


            if(ind >= 0 && ind < all.size()) {
                return 0;
            }

            if(all.size() == 0) dto.setLotId(0);
            else dto.setLotId(all.get(all.size()-1).getLotId()+1);
            all.add(dto);

            String outputJson = gson.toJson(all);

            try(FileWriter writer = new FileWriter(lotsFile)) {
                writer.write(outputJson);
            }

            return 1;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public LotDTO select(Object lotIdObj) {
        Long lotId = (Long) lotIdObj;

        try (FileReader reader = new FileReader(lotsFile)){
            List<LotDTO> all = gson.fromJson(reader, type);

            if(all == null) all = new ArrayList<>();

            LotDTO dto = new LotDTO();
            dto.setLotId(lotId);

            all.sort((x,y) -> Long.compare(x.getLotId(),y.getLotId()));
            int ind = Collections.binarySearch(all, dto, (x,y) -> Long.compare(x.getLotId(),y.getLotId()));
            if(ind < 0 || ind >= all.size()) return null;

            dto = all.get(ind);

            return dto;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public List<LotDTO> selectAll() {
        try (FileReader reader = new FileReader(lotsFile)){
            List<LotDTO> all = gson.fromJson(reader, type);
            if(all == null) all = new ArrayList<>();
            all.sort((x,y) -> Long.compare(x.getLotId(),y.getLotId()));

            return all;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public int update(LotDTO dto) {
        try {
            List<LotDTO> all;
            try (FileReader reader = new FileReader(lotsFile)){
                all = gson.fromJson(reader, type);
            }
            if(all == null) all = new ArrayList<>();

            all.sort((x,y) -> Long.compare(x.getLotId(), y.getLotId()));
            int ind = Collections.binarySearch(all, dto, (x,y) -> Long.compare(x.getLotId(),y.getLotId()));

            if(ind < 0 || ind >= all.size() || all.get(ind).getLotId() != dto.getLotId()) return 0;

            all.set(ind, dto);

            String outputJson = gson.toJson(all);
            try(FileWriter writer = new FileWriter(lotsFile)) {
                writer.write(outputJson);
            }
            return 1;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }

    public int delete(LotDTO dto) {
        try {
            List<LotDTO> all;
            try(FileReader reader = new FileReader(lotsFile)){
                all = gson.fromJson(reader, type);
            }

            if(all == null) all = new ArrayList<>();

            all.sort((x,y) -> Long.compare(x.getLotId(), y.getLotId()));
            int ind = Collections.binarySearch(all, dto, (x,y) -> Long.compare(x.getLotId(), y.getLotId()));

            if(ind < 0 || ind >= all.size() || all.get(ind).getLotId() != dto.getLotId()) return 0;

            dto = all.get(ind);

            all.remove(dto);

            String outputJson = gson.toJson(all);
            try(FileWriter writer = new FileWriter(lotsFile)) {
                writer.write(outputJson);
            }
            return 1;
        } catch (FileNotFoundException e) {
            logger.severe("admin json file was not founded");
            throw new RuntimeException(e.getMessage(), e);
        } catch (IOException e) {
            logger.severe("IO error! Something wrong with JSON files");
            throw new RuntimeException("JSON Files error:\n" + e.getMessage(), e);
        }
    }
}