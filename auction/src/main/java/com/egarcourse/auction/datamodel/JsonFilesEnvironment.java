package com.egarcourse.auction.datamodel;

import com.egarcourse.auction.datamodel.dataaccess.DataAccess;
import com.egarcourse.auction.datamodel.dataaccess.jsonaccess.ClientDAO;
import com.egarcourse.auction.datamodel.dataaccess.jsonaccess.AdministratorDAO;
import com.egarcourse.auction.datamodel.dataaccess.jsonaccess.BetDAO;
import com.egarcourse.auction.datamodel.dataaccess.jsonaccess.LotDAO;
import com.egarcourse.auction.datamodel.entities.AdministratorDTO;
import com.egarcourse.auction.datamodel.entities.BetDTO;
import com.egarcourse.auction.datamodel.entities.ClientDTO;
import com.egarcourse.auction.datamodel.entities.LotDTO;

import java.io.File;
import java.nio.file.Path;

public class JsonFilesEnvironment implements DataAccessEnvironment {
    private static final String rootPath = "./resources/";
    private static final String clientsPath = "clients.json";
    private static final String adminsPath = "admins.json";
    private static final String lotsPath = "lots.json";
    private static final String betsPath = "bets.json";


    private DataAccess<ClientDTO> clientDAO = new ClientDAO();
    private DataAccess<AdministratorDTO> adminDAO = new AdministratorDAO();
    private DataAccess<LotDTO> lotsDAO = new LotDAO();
    private DataAccess<BetDTO> betsDAO = new BetDAO();

    @Override
    public void init() {
        File pathFile = new File(rootPath);
        if(!pathFile.exists() && !pathFile.mkdir()) throw new RuntimeException("Cant create directory. Create it by yourself");

        clientDAO.init(rootPath + clientsPath);
        adminDAO.init(rootPath + adminsPath);
        lotsDAO.init(rootPath + lotsPath);
        betsDAO.init(rootPath + betsPath);
    }

    @Override
    public DataAccess<ClientDTO> getClientDAO() {
        return clientDAO;
    }

    @Override
    public DataAccess<AdministratorDTO> getAdminDAO() {
        return adminDAO;
    }

    @Override
    public DataAccess<LotDTO> getLotsDAO() {
        return lotsDAO;
    }

    @Override
    public DataAccess<BetDTO> getBetsDAO() {
        return betsDAO;
    }
}
